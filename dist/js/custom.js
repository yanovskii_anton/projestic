function setGapUnderHeader() {
	var headerHeight = $('.navbar-fixed-top').innerHeight();
	var layout = $('.layout');
	layout.css('paddingTop', headerHeight);
}

function makeAnimation(selector, effect, offset){
	$(selector).addClass("outscreen").viewportChecker({
	    classToAdd: 'inscreen animated '+effect, // Class to add to the elements when they are visible
	    offset: offset   
	   }); 
}

function makeDecoration(){
	var navItem = $('.js-nav-decoration > li');
	$('.js-nav-decoration').append( '<li class="js-nav-decorator"></li>' );
	navItem.on('mouseover', function(){
		var itemWidth = this.offsetWidth;
		var position = $(this).position();
		$('.js-nav-decorator').css('width', itemWidth).css('left', position.left^0);
	});
}




$( document ).ready(function() {
	setGapUnderHeader();
	makeDecoration();

	makeAnimation('.link-item', 'fadeInUp', 200);
	makeAnimation('.info-item:nth-child(2n-1)', 'slideInLeft', 200);
	makeAnimation('.info-item:nth-child(2n)', 'slideInRight', 200);

});

window.addEventListener("resize" || "orientationchange", function() {
  setGapUnderHeader();
}, false);