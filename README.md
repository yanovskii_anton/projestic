# projestic-test-page

Test task for slicing *.psd mockup.
Purpose: Responsive web page with using Bootstrap.

## Usage

1. Check out or download this project.
2. Run command in your terminal:

```
$ cd projestic.test
$ npm install
$ gulp
```

Then you can see the customized result by opening `index.html` in your browser.