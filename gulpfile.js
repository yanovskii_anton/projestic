/* gulpfile.js */
var 
    gulp = require('gulp'),
    connect = require('gulp-connect'),
    opn = require('opn'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps');

// source and distribution folder
var
    source = 'src/',
    dest = 'dist/';

// Bootstrap scss source
var bootstrapSass = {
        in: './node_modules/bootstrap-sass/'
    };

// fonts
var fonts = {
        in: [source + 'fonts/*.*', bootstrapSass.in + 'assets/fonts/**/*'],
        out: dest + 'fonts/'
    };

 // css source file: .scss files
var css = {
    in: source + 'scss/main.scss',
    out: dest + 'css/',
    watch: source + 'scss/**/*',
    sassOpts: {
        outputStyle: 'nested',
        precision: 8,
        errLogToConsole: true,
        includePaths: [bootstrapSass.in + 'assets/stylesheets']
    }
};


gulp.task('fonts', function () {
    return gulp
        .src(fonts.in)
        .pipe(gulp.dest(fonts.out));
});


// compile scss
gulp.task('sass', ['fonts'], function () {
    return gulp.src(css.in)
    
        .pipe(sourcemaps.init())
        .pipe(sass(css.sassOpts))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest(css.out))
        .pipe(connect.reload());
});


// launch local server
gulp.task('connect', function() {
  connect.server({
    root: './',
    livereload: true,
    port: 8888
  });
  opn('http://localhost:8888');
});


// work with html
gulp.task('html', function () {
  gulp.src('./*.html')
    .pipe(connect.reload());
});


// watcher
gulp.task('watch', function () {
  gulp.watch(['./*.html'], ['html']);
  gulp.watch(css.watch, ['sass']);
});


// default task
gulp.task('default', ['connect', 'watch', 'sass']);